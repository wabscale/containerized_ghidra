FROM openjdk:11-stretch

RUN apt update && apt install -y fonts-noto-mono && rm -rf /var/lib/apt/lists/

WORKDIR /opt/
RUN wget -q -O ghidra.zip "https://www.ghidra-sre.org/ghidra_9.0_PUBLIC_20190228.zip" && unzip ghidra.zip && rm ghidra.zip

COPY entrypoint.sh /root/entrypoint.sh
WORKDIR /root/

CMD [ "sh", "entrypoint.sh" ]
